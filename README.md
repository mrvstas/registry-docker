# Project for docker repository problem.

In this project i was try to understand problem with pushing image into private docker repository.

I'm using Ubuntu 18.04

At first we need to clone project into your local machine.

** git clone git@bitbucket.org:mrvstas/registry-docker.git **

After that you can start demo using vagrant or using real server.


# A. Start with vagrant (on your local machine)

## 1. Prepare vagrant configuration

I'm forward port 6080 in host machine, but you can change this in file Vagrantfile:

>   config.vm.network "forwarded_port", guest: 80, host:  6080

## 2. Start vagrant
`
$ vagrant up
`
## 3. Result
  After that we have unsecure docker registry available on port 6080 your host machine.
  In my localnet it was 192.168.3.131:6080

## 4. How to use unsecure docker registry
The another machine in my localnet i'm using for testing docker registry.
At first create or change file /etc/docker/daemon.json on your test machine.
Use your docker registry address and port.
  
>  {
>    "insecure-registries" : [ "192.168.3.131:6080" ]
>  }

Restart docker on your test machine
`
$ sudo service docker restart
`
 
## 5. Login to docker repository
Use test account.

User: testuser

Password: testpass

`	 
$ docker login 192.168.3.131:6080
`
     
## 6. Prepare image
Add tag for image. I'm using ubuntu image

` 
$ docker tag ubuntu 192.168.3.131:6080/my-ubuntu
` 

## 7. Push image to our docker registry
`
$ docker push 192.168.3.131:6080/my-ubuntu
`
 
# B. Start with real server with Ubuntu 18.04

## 1. Prepare ansible
You must define IP address your server in file hosts/servers
For example your address 212.44.32.12 and domain registry.dev.example.net

>  registry  ansible_ssh_user=root ansible_ssh_host=212.44.32.12

## 2. Start ansible playbook
`
$ ansible-playbook  -i hosts/servers playbook.yml
`
## 3. Result
After that we have unsecure docker registry available on port 80 your server.
I am use empty virtual server with base installation Ubuntu 18.04

## 4. How to use unsecure docker registry
At first create or change file /etc/docker/daemon.json  using your docker registry address and port.
>  {
>    "insecure-registries" : [ "registry.dev.example.net" ]
>  }

Restart docker on your local machine

`
$ sudo service docker restart
`
## 5. Login to docker repository
Use test account.

User: testuser

Password: testpass

`	 
$ docker login registry.dev.example.net
`

## 6. Add tag for image. I'm using ubuntu image
`
$ docker tag ubuntu registry.dev.example.net/my-ubuntu
`
## 7. Push image
`
$ docker push registry.dev.example.net/my-ubuntu
`
***

And now we have problem... :(((


```
$ docker push 192.168.3.131:6080/my-ubuntu
The push refers to a repository [192.168.3.131:6080/my-ubuntu]
2de391e51d73: Retrying in 14 seconds
d73dd9e65295: Retrying in 14 seconds
686245e78935: Retrying in 14 seconds
d7ff1dc646ba: Retrying in 14 seconds
644879075e24: Retrying in 14 seconds
```

It's working, but really slow. And I don't know how to fix this.



